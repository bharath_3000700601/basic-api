package com.example.demo.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
public class TopicService {
 
	
	private List<Topic> topics= new ArrayList<>(Arrays.asList(
			  new Topic("Spring","Spring Framework","Spring Framework description"),
			  new Topic("Java","Core Java","Core Java Description"),
			  new Topic("Java Script","javascript","javadescription")
			  ));
	
	public List<Topic> getAllTopics() {
		return topics;
	}
	
	
	public Topic getTopic(String id) {
		return topics.parallelStream().filter(t -> t.getId().equals(id)).findFirst().get();
	}


	public void addTopic(Topic topic) {
		// TODO Auto-generated method stub
		topics.add(topic);
		for(int i=0;i<4;i++)
			System.out.println(topics.get(i).getId());
	}
}
