package com.example.demo.calculator;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Multiplier {

	@RequestMapping(method=RequestMethod.POST,value="/mult")
	 public String multiply(@RequestBody Values value)
	{
		return "Multiplication of given numbers is " + (value.getA()*value.getB());
	}
}
