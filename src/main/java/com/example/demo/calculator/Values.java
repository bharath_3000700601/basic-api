package com.example.demo.calculator;

public class Values {

	float a;
	char operator;
	float b;
	
	public Values() {
		
	}

	public Values(float a, char operator, float b) {
		super();
		this.a = a;
		this.operator = operator;
		this.b = b;
	}

	public float getA() {
		return a;
	}

	public void setA(float a) {
		this.a = a;
	}

	public char getOperator() {
		return operator;
	}

	public void setOperator(char operator) {
		this.operator = operator;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}

	
	
	
}
