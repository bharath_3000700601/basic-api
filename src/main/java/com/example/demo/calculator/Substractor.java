package com.example.demo.calculator;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Substractor {
	@RequestMapping(method=RequestMethod.POST,value="/sub")
	 public String sub(@RequestBody Values value)
	{
		return "substraction of given numbers is " + (value.getA()-value.getB());
	}
}
