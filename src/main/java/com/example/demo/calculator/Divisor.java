package com.example.demo.calculator;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Divisor {

	@RequestMapping(method=RequestMethod.POST,value="/div")
	 public String divide(@RequestBody Values value)
	{
		if(value.getB()!=0)
		return "Division of given numbers is " + (value.getA()/value.getB());
		else
			return "Please enter valid number";
	}
}
