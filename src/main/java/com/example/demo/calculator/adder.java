package com.example.demo.calculator;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class adder {
	
	@RequestMapping(method=RequestMethod.POST,value="/adder")
	 public String add(@RequestBody Values value)
	{
		return "Addition of given numbers is " + (value.getA()+value.getB());
	}

}
